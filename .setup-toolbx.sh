RUST_VERSION=1.79.0
CARGO_MAKE_VERSION=0.37.14
RUSTY_HOOK_VERSION=0.11.2

sudo tee /etc/profile.d/pone-rust.sh <<'EOF'
export RUSTUP_HOME=/opt/rust
export CARGO_HOME=/opt/rust
export PATH="$CARGO_HOME/bin:$PATH"
EOF
. /etc/profile.d/pone-rust.sh
sudo mkdir -p "$RUSTUP_HOME"
sudo chown `id -u`:`id -g` "$RUSTUP_HOME"

sudo dnf -y install rustup gcc cmake podman-remote
rustup-init -y --default-toolchain "$RUST_VERSION" --no-modify-path
cargo install "cargo-make@$CARGO_MAKE_VERSION" "rusty-hook@$RUSTY_HOOK_VERSION"
