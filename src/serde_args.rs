use std::collections::HashSet;

use serde::de::{DeserializeSeed, Visitor};
use serde::{forward_to_deserialize_any, Deserialize, Deserializer};

pub fn from_string_iter<'t, T: Deserialize<'t>>(
    iter: &mut dyn Iterator<Item = String>,
) -> Result<T, Error> {
    let mut de = DeArgs {
        args: iter.collect(),
        state: DeState::Basic,
    };
    de.args.reverse();
    let t = T::deserialize(&mut de)?;
    if de.args.is_empty() {
        Ok(t)
    } else {
        Err(Error::ExtraArguments(de.args.join(" ")))
    }
}

#[derive(Clone, Debug)]
enum DeState {
    Basic,
    Options {
        options: HashSet<String>,
        skipped: Vec<String>,
    },
    Option {
        option: String,
        options: HashSet<String>,
        skipped: Vec<String>,
    },
    OptionItem,
}
impl DeState {
    fn base_option(option: &str) -> &str {
        option.strip_suffix("-json").unwrap_or(option)
    }

    fn has_option(options: &HashSet<String>, option: &str) -> bool {
        options.contains(Self::base_option(option))
    }

    fn identifier<'v>(&self, value: &'v str) -> Result<&'v str, Error> {
        match self {
            DeState::Options { .. } => match value.strip_prefix("--") {
                Some(name) => Ok(Self::base_option(name)),
                None => Err(Error::ExpectedOption(value.to_string())),
            },
            _ => Ok(&value),
        }
    }
}

struct DeArgs {
    args: Vec<String>,
    state: DeState,
}
impl DeArgs {
    fn with_state<R, F: FnOnce(&mut Self) -> R>(&mut self, state: DeState, f: F) -> R {
        let args = match state {
            DeState::OptionItem => vec![],
            _ => std::mem::replace(&mut self.args, vec![]),
        };
        let mut de = DeArgs { args, state };
        let result = f(&mut de);
        match de.state {
            DeState::Basic => {
                self.args = de.args;
            }
            DeState::Options {
                ref mut skipped, ..
            } => {
                skipped.reverse();
                de.args.append(skipped);
                self.args = de.args;
            }
            DeState::Option {
                ref mut skipped, ..
            } => {
                skipped.reverse();
                de.args.append(skipped);
                self.args = de.args;
            }
            DeState::OptionItem => {
                self.args.append(&mut de.args);
            }
        }
        result
    }
}
impl<'de, 'a> Deserializer<'de> for &'a mut DeArgs {
    type Error = Error;

    fn deserialize_any<V: Visitor<'de>>(self, _visitor: V) -> Result<V::Value, Self::Error> {
        unimplemented!()
    }

    fn deserialize_bool<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        visitor.visit_bool(true)
    }

    fn deserialize_u32<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match self.args.pop() {
            Some(value) => visitor.visit_u32(value.parse()?),
            None => Err(Error::MissingArgument),
        }
    }

    fn deserialize_u64<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match self.args.pop() {
            Some(value) => visitor.visit_u64(value.parse()?),
            None => Err(Error::MissingArgument),
        }
    }

    fn deserialize_string<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match self.args.pop() {
            Some(value) => visitor.visit_string(value),
            None => Err(Error::MissingArgument),
        }
    }

    fn deserialize_identifier<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match self.args.pop() {
            Some(value) => visitor.visit_str(self.state.identifier(&value)?),
            None => Err(Error::MissingArgument),
        }
    }

    fn deserialize_option<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        visitor.visit_some(self)
    }

    fn deserialize_tuple<V: Visitor<'de>>(
        self,
        len: usize,
        visitor: V,
    ) -> Result<V::Value, Self::Error> {
        self.with_state(DeState::Basic, move |de| {
            visitor.visit_seq(DeSeq {
                de,
                count: Some(len),
            })
        })
    }

    fn deserialize_tuple_struct<V: Visitor<'de>>(
        self,
        _name: &'static str,
        len: usize,
        visitor: V,
    ) -> Result<V::Value, Self::Error> {
        self.with_state(DeState::Basic, move |de| {
            visitor.visit_seq(DeSeq {
                de,
                count: Some(len),
            })
        })
    }

    fn deserialize_struct<V: Visitor<'de>>(
        self,
        _name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error> {
        let options = fields
            .into_iter()
            .map(|field| format!("--{}", field))
            .collect();
        self.with_state(
            DeState::Options {
                options,
                skipped: vec![],
            },
            move |de| visitor.visit_map(DeOptions { de, option: None }),
        )
    }

    fn deserialize_seq<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match self.state {
            DeState::Option { .. } => visitor.visit_seq(DeOptionSeq {
                de: self,
                first: true,
            }),
            _ => self.with_state(DeState::Basic, move |de| {
                visitor.visit_seq(DeSeq { de, count: None })
            }),
        }
    }

    fn deserialize_enum<V: Visitor<'de>>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error> {
        self.with_state(DeState::Basic, move |de| visitor.visit_enum(DeEnum { de }))
    }

    forward_to_deserialize_any! {
        i8 i16 i32 i64 i128 u8 u16 u128 f32 f64 char str
        bytes byte_buf unit unit_struct newtype_struct
        map ignored_any
    }
}

struct DeSeq<'a> {
    de: &'a mut DeArgs,
    count: Option<usize>,
}
impl<'a, 'de> serde::de::SeqAccess<'de> for DeSeq<'a> {
    type Error = Error;

    fn next_element_seed<S>(&mut self, seed: S) -> Result<Option<S::Value>, Error>
    where
        S: DeserializeSeed<'de>,
    {
        match (self.count, self.de.args.last()) {
            (Some(count), _) if count > 0 => {
                self.count = Some(count - 1);
                seed.deserialize(&mut *self.de).map(Some)
            }
            (None, Some(_)) => seed.deserialize(&mut *self.de).map(Some),
            _ => Ok(None),
        }
    }

    fn size_hint(&self) -> Option<usize> {
        self.count
    }
}

struct DeOptions<'a> {
    de: &'a mut DeArgs,
    option: Option<DeState>,
}
impl<'a, 'de> serde::de::MapAccess<'de> for DeOptions<'a> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Error>
    where
        K: DeserializeSeed<'de>,
    {
        let DeArgs { args, state } = &mut self.de;
        if let DeState::Options {
            options,
            ref mut skipped,
        } = state
        {
            if let Some(index) = args
                .iter()
                .rposition(|arg| DeState::has_option(options, arg))
            {
                let mut split = args.split_off(index + 1);
                let option = args.last().unwrap();
                split.reverse();
                skipped.append(&mut split);
                self.option = Some(DeState::Option {
                    option: option.to_string(),
                    options: options.clone(),
                    skipped: vec![],
                });
                seed.deserialize(&mut *self.de).map(Some)
            } else {
                Ok(None)
            }
        } else {
            Err(Error::InvalidState(format!("{:?}", state)))
        }
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Error>
    where
        V: DeserializeSeed<'de>,
    {
        match self.option.take() {
            Some(option @ DeState::Option { .. }) => {
                self.de.with_state(option, move |de| seed.deserialize(de))
            }
            state => Err(Error::InvalidState(format!("{state:?}"))),
        }
    }
}

struct DeOptionSeq<'a> {
    de: &'a mut DeArgs,
    first: bool,
}
impl<'a, 'de> serde::de::SeqAccess<'de> for DeOptionSeq<'a> {
    type Error = Error;

    fn next_element_seed<S>(&mut self, seed: S) -> Result<Option<S::Value>, Error>
    where
        S: DeserializeSeed<'de>,
    {
        let DeArgs { args, state } = &mut self.de;
        if let DeState::Option {
            option,
            options,
            ref mut skipped,
        } = state
        {
            let option = if self.first {
                self.first = false;
                option.clone()
            } else if let Some(index) = args
                .iter()
                .rposition(|arg| DeState::base_option(arg) == DeState::base_option(option))
            {
                let mut split = args.split_off(index + 1);
                split.reverse();
                skipped.append(&mut split);
                args.pop().unwrap()
            } else {
                return Ok(None);
            };
            let item = if let Some(index) = args
                .iter()
                .rposition(|arg| DeState::has_option(options, arg))
            {
                args.split_off(index + 1)
            } else {
                std::mem::replace(args, vec![])
            };
            self.de.with_state(DeState::OptionItem, move |de| {
                de.args = item;

                if option.ends_with("-json") {
                    if let Some(arg) = de.args.pop() {
                        let read = std::io::Cursor::new(arg);
                        let mut de = serde_json::Deserializer::from_reader(read);
                        let result = seed.deserialize(&mut de)?;
                        de.end()?;
                        Ok(Some(result))
                    } else {
                        Err(Error::MissingArgument)
                    }
                } else {
                    seed.deserialize(de).map(Some)
                }
            })
        } else {
            Err(Error::InvalidState(format!("{:?}", state)))
        }
    }
}

struct DeEnum<'a> {
    de: &'a mut DeArgs,
}

impl<'a, 'de> serde::de::EnumAccess<'de> for DeEnum<'a> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<S>(self, seed: S) -> Result<(S::Value, Self::Variant), Self::Error>
    where
        S: DeserializeSeed<'de>,
    {
        Ok((seed.deserialize(&mut *self.de)?, self))
    }
}

impl<'a, 'de> serde::de::VariantAccess<'de> for DeEnum<'a> {
    type Error = Error;

    fn unit_variant(self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn newtype_variant_seed<S>(self, seed: S) -> Result<S::Value, Self::Error>
    where
        S: DeserializeSeed<'de>,
    {
        seed.deserialize(self.de)
    }

    fn tuple_variant<V: Visitor<'de>>(
        self,
        _len: usize,
        _visitor: V,
    ) -> Result<V::Value, Self::Error> {
        unimplemented!()
    }
    fn struct_variant<V: Visitor<'de>>(
        self,
        _fields: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value, Self::Error> {
        unimplemented!()
    }
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error(transparent)]
    JsonError(#[from] serde_json::Error),

    #[error("Invalid state: {0}")]
    InvalidState(String),
    #[error("Extra argument: {0}")]
    ExtraArguments(String),
    #[error("Argument missing")]
    MissingArgument,
    #[error("Expected option: {0}")]
    ExpectedOption(String),
    #[error("{0}")]
    Custom(String),
}
impl serde::de::Error for Error {
    fn custom<T: std::fmt::Display>(msg: T) -> Self {
        Error::Custom(format!("{}", msg))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! svec {
        ($($x:expr),*) => (vec![$($x.to_string()),*]);
    }
    macro_rules! evec {
        ($($name:literal=$value:expr),*) => (vec![$(($name.to_string(), $value.to_string())),*]);
    }

    fn assert_de<T: Deserialize<'static> + PartialEq + std::fmt::Debug + Sized + 'static>(
        expected: T,
        args: Vec<String>,
    ) -> Result<(), Error> {
        assert_eq!(expected, from_string_iter(&mut args.into_iter())?);
        Ok(())
    }

    #[test]
    fn test_string() -> Result<(), Error> {
        assert_de("a".to_string(), svec!["a"])
    }

    #[derive(Deserialize, PartialEq, Debug)]
    #[serde(rename_all = "lowercase")]
    enum Choice {
        One,
        Two,
        Three,
    }
    #[test]
    fn test_enum() -> Result<(), Error> {
        assert_de(Choice::Two, svec!["two"])
    }

    #[derive(Deserialize, PartialEq, Debug)]
    enum Variant {
        Seed(String),
    }
    #[test]
    fn test_variant_seed() -> Result<(), Error> {
        assert_de(Variant::Seed("a".into()), svec!["Seed", "a"])
    }

    #[test]
    fn test_vec() -> Result<(), Error> {
        assert_de(vec!["a".to_string(), "b".to_string()], svec!["a", "b"])
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct Command(String, Vec<String>);
    #[test]
    fn test_command() -> Result<(), Error> {
        assert_de(
            Command("cmd".into(), svec!["--flag", "--opt", "a", "b"]),
            svec!["cmd", "--flag", "--opt", "a", "b"],
        )?;
        assert_de(Command("cmd".into(), vec![]), svec!["cmd"])?;
        Ok(())
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct Options {
        string: Option<String>,
        choice: Option<Choice>,
    }
    #[test]
    fn test_options_some() -> Result<(), Error> {
        assert_de(
            Options {
                string: Some("a_string".into()),
                choice: Some(Choice::Three),
            },
            svec!["--string", "a_string", "--choice", "three"],
        )
    }
    #[test]
    fn test_options_none() -> Result<(), Error> {
        assert_de(
            Options {
                string: None,
                choice: None,
            },
            svec![],
        )
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct OptionsOrCommand(Options, Command);
    #[test]
    fn test_options_command_some() -> Result<(), Error> {
        assert_de(
            OptionsOrCommand(
                Options {
                    string: Some("a_string".into()),
                    choice: Some(Choice::Three),
                },
                Command("cmd".into(), svec!["--flag", "--opt", "a", "b"]),
            ),
            svec!["--string", "a_string", "--choice", "three", "cmd", "--flag", "--opt", "a", "b"],
        )
    }
    #[test]
    fn test_options_command_none() -> Result<(), Error> {
        assert_de(
            OptionsOrCommand(
                Options {
                    string: None,
                    choice: None,
                },
                Command("cmd".into(), svec!["--flag", "--opt", "a", "b"]),
            ),
            svec!["cmd", "--flag", "--opt", "a", "b"],
        )
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct OptionsCommandOption {
        command: Vec<Command>,
        string: Vec<String>,
    }
    #[test]
    fn test_options_commnad_option() -> Result<(), Error> {
        assert_de(
            OptionsCommandOption {
                command: vec![
                    Command("a".into(), svec!["b", "c"]),
                    Command("d".into(), svec!["e", "f"]),
                    Command("g".into(), svec!["h", "i"]),
                ],
                string: svec!["x", "y"],
            },
            svec![
                "--command",
                "a",
                "b",
                "c",
                "--command",
                "d",
                "e",
                "f",
                "--string",
                "x",
                "--command",
                "g",
                "h",
                "i",
                "--string",
                "y"
            ],
        )
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct CommandOptions {
        uid: Option<u32>,
        #[serde(default)]
        env: Vec<(String, String)>,
        #[serde(default, rename = "env-clear")]
        env_clear: bool,
    }
    #[derive(Deserialize, PartialEq, Debug)]
    struct CommandWithOptions(CommandOptions, String, Vec<String>);
    #[test]
    fn test_command_with_options() -> Result<(), Error> {
        assert_de(
            CommandWithOptions(
                CommandOptions {
                    uid: None,
                    env: vec![],
                    env_clear: false,
                },
                "executable".into(),
                svec!["--flag"],
            ),
            svec!["executable", "--flag"],
        )?;
        assert_de(
            CommandWithOptions(
                CommandOptions {
                    uid: Some(1000),
                    env: vec![],
                    env_clear: false,
                },
                "executable".into(),
                svec!["--flag"],
            ),
            svec!["--uid", 1000, "executable", "--flag"],
        )?;
        assert_de(
            CommandWithOptions(
                CommandOptions {
                    uid: None,
                    env: evec!["AA" = "aa", "BB" = "bb"],
                    env_clear: true,
                },
                "executable".into(),
                svec!["--flag"],
            ),
            svec![
                "--env-clear",
                "--env",
                "AA",
                "aa",
                "--env",
                "BB",
                "bb",
                "executable",
                "--flag"
            ],
        )?;
        Ok(())
    }

    #[derive(Deserialize, PartialEq, Debug)]
    struct JsonChildOptions {
        number: usize,
    }
    #[derive(Deserialize, PartialEq, Debug)]
    struct JsonParentOptions {
        child: Vec<JsonChildOptions>,
    }
    #[test]
    fn test_json_options() -> Result<(), Error> {
        assert_de(
            JsonParentOptions {
                child: vec![
                    JsonChildOptions { number: 1 },
                    JsonChildOptions { number: 2 },
                    JsonChildOptions { number: 3 },
                ],
            },
            svec![
                "--child-json",
                r#"{"number": 1}"#,
                "--child",
                "--number",
                "2",
                "--child-json",
                r#"{"number": 3}"#
            ],
        )?;
        assert_de(
            JsonParentOptions {
                child: vec![
                    JsonChildOptions { number: 1 },
                    JsonChildOptions { number: 2 },
                    JsonChildOptions { number: 3 },
                ],
            },
            svec![
                "--child",
                "--number",
                "1",
                "--child-json",
                r#"{"number": 2}"#,
                "--child",
                "--number",
                "3"
            ],
        )?;
        Ok(())
    }
}
