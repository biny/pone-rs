use std::collections::HashMap;

use act_zero::{call, send, Actor, ActorResult, Addr, AddrLike, Produces};
use futures::channel::oneshot;
use tokio::signal::unix::{signal, SignalKind};

use crate::for_subsystem::*;

pub trait CoreSubsystem {
    fn init_core(&mut self);
}
impl CoreSubsystem for SubsystemsBuild {
    fn init_core(&mut self) {
        self.spawn_actor(|_build, system| Core {
            system,
            exits: HashMap::new(),
            pid_exit_hook: None,
            waitpids: HashMap::new(),
        });
    }
}

#[async_trait::async_trait]
pub trait CoreSubsystemRuntime {
    async fn waitpid(&self, pid: u32) -> Result<Option<PidExit>, Error>;
}
#[async_trait::async_trait]
impl CoreSubsystemRuntime for SubsystemsRuntime {
    async fn waitpid(&self, pid: u32) -> Result<Option<PidExit>, Error> {
        let core: Addr<Core> = self.get();
        let rx = call!(core.waitpid(pid)).await?;
        Ok(rx.await?)
    }
}

pub(crate) struct Core {
    system: Addr<System>,
    exits: HashMap<u32, PidExit>,
    pid_exit_hook: Option<Addr<dyn PidExitHook>>,
    waitpids: HashMap<u32, oneshot::Sender<Option<PidExit>>>,
}

impl Core {
    pub(crate) async fn set_pid_exit_hook(
        &mut self,
        hook: Addr<dyn PidExitHook>,
    ) -> ActorResult<()> {
        self.pid_exit_hook.replace(hook);
        Produces::ok(())
    }
    async fn waitpid(&mut self, pid: u32) -> ActorResult<oneshot::Receiver<Option<PidExit>>> {
        let (tx, rx) = oneshot::channel();
        if let Some(exit) = self.exits.remove(&pid) {
            tx.send(Some(exit)).unwrap();
        } else {
            self.waitpids.insert(pid, tx);
        }
        Produces::ok(rx)
    }

    async fn sigchld(&mut self, exits: Result<Vec<PidExit>, std::io::Error>) -> ActorResult<()> {
        for exit in exits? {
            let mut unhandled = Some(exit);
            while let Some(exit) = unhandled {
                if let Some(tx) = self.waitpids.remove(&exit.pid) {
                    unhandled = match tx.send(Some(exit)) {
                        Ok(_) => None,
                        Err(exit) => exit,
                    };
                } else if let Some(hook) = &self.pid_exit_hook {
                    send!(hook.pid_exit(exit));
                    unhandled = None;
                } else if self.waitpids.is_empty() {
                    self.exits.insert(exit.pid, exit);
                    unhandled = None;
                } else {
                    self.send(&self.system, vec![Event::Info(Box::new(exit))]);
                    unhandled = None;
                }
            }
        }
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Subsystem for Core {
    async fn stop(&mut self) -> ActorResult<()> {
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Actor for Core {
    async fn started(&mut self, addr: Addr<Self>) -> ActorResult<()>
    where
        Self: Sized,
    {
        if std::process::id() == 1 {
            for kind in [
                SignalKind::terminate(),
                SignalKind::interrupt(),
                SignalKind::quit(),
            ] {
                let system_child = self.system.clone();
                let mut stream = signal(kind)?;
                let fut = async move {
                    loop {
                        if let None = stream.recv().await {
                            break;
                        }
                        send!(system_child.stop());
                    }
                };
                addr.send_fut(fut)
            }
        }
        let addr_child = addr.clone();
        let mut stream = signal(SignalKind::child())?;
        let fut = async move {
            loop {
                if let None = stream.recv().await {
                    break;
                }
                let exits = waitpids();
                send!(addr_child.sigchld(exits));
            }
        };
        addr.send_fut(fut);
        Produces::ok(())
    }

    async fn error(&mut self, error: act_zero::ActorError) -> bool {
        self.send(&self.system, vec![Event::ActorError(error)]);
        true
    }
}

fn waitpids() -> Result<Vec<PidExit>, std::io::Error> {
    let mut exits = vec![];
    let mut status = 0;
    loop {
        match unsafe { libc::waitpid(-1, &mut status, libc::WNOHANG) } {
            0 => break, // nothing available
            pid if pid > 0 => exits.push(PidExit {
                pid: pid as u32,
                reason: PidExitReason::from(status),
            }),
            _ => {
                let error = std::io::Error::last_os_error();
                if error.kind() == std::io::ErrorKind::Interrupted {
                    continue;
                }
                if error.raw_os_error() == Some(libc::ECHILD) {
                    break;
                }
                return Err(error);
            }
        }
    }
    Ok(exits)
}

#[async_trait::async_trait]
pub(crate) trait PidExitHook: Actor {
    async fn pid_exit(&mut self, exit: PidExit) -> ActorResult<()>;
}

#[derive(Debug, Clone, PartialEq)]
pub struct PidExit {
    pub pid: u32,
    pub reason: PidExitReason,
}

#[derive(Debug, Clone, PartialEq)]
pub enum PidExitReason {
    Code(i32),
    Signal(i32),
    Unknown(i32),
}
impl From<i32> for PidExitReason {
    fn from(status: libc::c_int) -> Self {
        if libc::WIFEXITED(status) {
            PidExitReason::Code(libc::WEXITSTATUS(status))
        } else if libc::WIFSIGNALED(status) {
            PidExitReason::Signal(libc::WTERMSIG(status))
        } else {
            PidExitReason::Unknown(status)
        }
    }
}
