mod subsystems;

mod core;
pub(crate) use crate::core::{Core, PidExitHook};
pub use crate::core::{CoreSubsystem, CoreSubsystemRuntime, PidExit, PidExitReason};

mod children;
pub(crate) use crate::children::Children;
pub use crate::children::{ChildExit, ChildrenSubsystem, ChildrenSubsystemRuntime};

mod nng_children;
pub use crate::nng_children::{
    NngChildrenService, NngChildrenSubsystem, NngChildrenSubsystemRuntime,
};

mod http;
pub use crate::http::{HttpService, HttpSubsystem, HttpSubsystemRuntime};

mod cron;
pub use crate::cron::{CronService, CronSubsystem, CronSubsystemRuntime};

mod nng_stream;
pub mod serde_args;
mod serde_http;

pub mod for_main {
    pub use crate::subsystems::SubsystemsBuild;
    pub use crate::Error;
}
pub(crate) mod for_subsystem {
    pub(crate) use crate::subsystems::{
        Event, Subsystem, SubsystemsBuild, SubsystemsRuntime, System,
    };
    pub(crate) use crate::Error;
}

use std::collections::HashMap;
use std::os::unix::process::CommandExt;

#[macro_export]
macro_rules! cmd {
    ($cmd:expr) => (Command(CommandOptions::default(), $cmd.to_string(), vec![]));
    ($cmd:expr, $($arg:expr),+) => (Command(CommandOptions::default(), $cmd.to_string(), vec![$($arg.to_string()),*]));
}

#[derive(serde::Deserialize, Default, PartialEq, Clone, Debug)]
pub struct CommandOptions {
    uid: Option<String>,
    gid: Option<String>,
    #[serde(default, rename = "env-clear")]
    env_clear: bool,
    #[serde(default)]
    env: Vec<(String, String)>,
    #[serde(default, rename = "no-env")]
    no_env: Vec<String>,
    #[serde(default, rename = "stop-signal")]
    stop_signal: StopSignal,
}
#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub struct Command(CommandOptions, String, Vec<String>);
impl From<Command> for CommandConfig {
    fn from(command: Command) -> Self {
        let Command(
            CommandOptions {
                uid,
                gid,
                env_clear,
                env,
                no_env,
                stop_signal,
            },
            program,
            args,
        ) = command;
        Self {
            program,
            args,
            uid,
            gid,
            env_clear,
            envs: env.into_iter().collect(),
            no_env,
            stop_signal,
        }
    }
}

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub enum StopSignal {
    Terminate,
    Interrupt,
    Quit,
}
impl Default for StopSignal {
    fn default() -> Self {
        StopSignal::Terminate
    }
}
impl StopSignal {
    fn as_raw(&self) -> libc::c_int {
        match self {
            StopSignal::Terminate => libc::SIGTERM,
            StopSignal::Interrupt => libc::SIGINT,
            StopSignal::Quit => libc::SIGQUIT,
        }
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct CommandConfig {
    program: String,
    args: Vec<String>,
    uid: Option<String>,
    gid: Option<String>,
    env_clear: bool,
    envs: HashMap<String, String>,
    no_env: Vec<String>,
    stop_signal: StopSignal,
}
impl CommandConfig {
    fn env<K: Into<String>, V: Into<String>>(&mut self, key: K, value: V) {
        self.envs.insert(key.into(), value.into());
    }
}
impl From<&CommandConfig> for std::process::Command {
    fn from(command: &CommandConfig) -> Self {
        let mut result = std::process::Command::new(&command.program);
        result.args(&command.args);
        if command.env_clear {
            result.env_clear();
        }
        result.envs(&command.envs);
        for env_key in &command.no_env {
            result.env_remove(env_key);
        }
        if let Some(uid) = &command.uid {
            let uid = match uid.parse::<u32>() {
                Ok(uid) => uid,
                Err(_) => unsafe {
                    let c_name = std::ffi::CString::new(uid.as_str()).expect("no zero bytes");
                    (*libc::getpwnam(c_name.as_ptr())).pw_uid
                },
            };
            result.uid(uid);
        }
        if let Some(gid) = &command.gid {
            let gid = match gid.parse::<u32>() {
                Ok(gid) => gid,
                Err(_) => unsafe {
                    let c_name = std::ffi::CString::new(gid.as_str()).expect("no zero bytes");
                    (*libc::getgrnam(c_name.as_ptr())).gr_gid
                },
            };
            result.gid(gid);
        }
        result
    }
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    BoxedStdError(#[from] Box<dyn std::error::Error + Send + Sync + 'static>),
    #[error(transparent)]
    ActorError(#[from] futures::channel::oneshot::Canceled),
    #[error(transparent)]
    WaitError(#[from] tokio::sync::watch::error::RecvError),
    #[error(transparent)]
    ArgsError(#[from] serde_args::Error),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
    #[error(transparent)]
    NngError(#[from] nng::Error),
    #[error(transparent)]
    AddrParseError(#[from] std::net::AddrParseError),
    #[error(transparent)]
    HttpInvalidMethod(#[from] hyper::http::method::InvalidMethod),
    #[error(transparent)]
    CronError(#[from] ::cron::error::Error),
    #[error("ChildError: {0:?}")]
    ChildError(ChildExit),
    #[error("Stopping")]
    Stopping,
}
