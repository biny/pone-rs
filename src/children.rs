use std::collections::HashMap;

use act_zero::{call, send, upcast, Actor, ActorResult, Addr, Produces};

use crate::for_subsystem::*;
use crate::{
    CommandConfig, Core, CoreSubsystem, CoreSubsystemRuntime, Error, PidExit, PidExitHook,
    PidExitReason,
};

pub trait ChildrenSubsystem: CoreSubsystem {
    fn init_children(&mut self);
}
impl ChildrenSubsystem for SubsystemsBuild {
    fn init_children(&mut self) {
        self.init_core();
        let core = self.get();
        self.spawn_actor(|_build, system| Children {
            system,
            core,
            child_exit_hook: None,
            children: HashMap::new(),
            last_exit: None,
            stopping: false,
        });
    }
}

#[async_trait::async_trait]
pub trait ChildrenSubsystemRuntime: CoreSubsystemRuntime {
    async fn start_simple<C: Into<CommandConfig> + Send + 'static>(
        &self,
        command: C,
    ) -> Result<(), Error>;
    async fn last_exit(&self) -> Result<Option<ChildExit>, Error>;
}
#[async_trait::async_trait]
impl ChildrenSubsystemRuntime for SubsystemsRuntime {
    async fn start_simple<C: Into<CommandConfig> + Send + 'static>(
        &self,
        command: C,
    ) -> Result<(), Error> {
        let children: Addr<Children> = self.get();
        Ok(call!(children.start_simple(command)).await??)
    }

    async fn last_exit(&self) -> Result<Option<ChildExit>, Error> {
        let children: Addr<Children> = self.get();
        Ok(call!(children.last_exit()).await?)
    }
}

pub struct Children {
    system: Addr<System>,
    core: Addr<Core>,
    child_exit_hook: Option<Addr<dyn ChildExitHook>>,
    children: HashMap<u32, (CommandConfig, std::process::Child)>,
    last_exit: Option<ChildExit>,
    stopping: bool,
}
impl Children {
    pub(crate) async fn start_simple<C: Into<CommandConfig>>(
        &mut self,
        command: C,
    ) -> ActorResult<std::io::Result<()>> {
        if self.stopping {
            Err(Error::Stopping)?;
        }
        let command = command.into();
        let child = Into::<std::process::Command>::into(&command).spawn()?;
        self.children.insert(child.id(), (command.clone(), child));
        self.send(
            &self.system,
            vec![Event::Info(Box::new(command)), Event::Running],
        );
        Produces::ok(Ok(()))
    }

    pub(crate) async fn start_limited(
        &mut self,
        max: usize,
        command: CommandConfig,
    ) -> ActorResult<std::io::Result<()>> {
        let child_count = self
            .children
            .iter()
            .filter(|(_pid, (child, _child))| child == &command)
            .count();
        if child_count >= max {
            return Produces::ok(Ok(()));
        }
        self.start_simple(command).await
    }

    pub(crate) async fn last_exit(&mut self) -> ActorResult<Option<ChildExit>> {
        Produces::ok(self.last_exit.take())
    }
}
#[async_trait::async_trait]
impl Subsystem for Children {
    async fn stop(&mut self) -> ActorResult<()> {
        self.stopping = true;
        for (command, child) in self.children.values() {
            let signal = command.stop_signal.as_raw();
            unsafe {
                libc::kill(child.id() as libc::pid_t, signal);
            }
        }
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Actor for Children {
    async fn started(&mut self, addr: Addr<Self>) -> ActorResult<()>
    where
        Self: Sized,
    {
        send!(self.core.set_pid_exit_hook(upcast!(addr)));
        Produces::ok(())
    }

    async fn error(&mut self, error: act_zero::ActorError) -> bool {
        self.send(&self.system, vec![Event::ActorError(error)]);
        true
    }
}
#[async_trait::async_trait]
impl PidExitHook for Children {
    async fn pid_exit(&mut self, exit: PidExit) -> ActorResult<()> {
        let exit = if let Some((command, _)) = self.children.remove(&exit.pid) {
            ChildExit::Child(command, exit)
        } else {
            ChildExit::Unknown(exit)
        };
        self.send(
            &self.system,
            vec![
                Event::Info(Box::new(exit.clone())),
                if self.children.is_empty() {
                    Event::Idle
                } else {
                    Event::Running
                },
            ],
        );
        if let Some(hook) = &self.child_exit_hook {
            send!(hook.child_exit(exit.clone()));
        }
        self.last_exit = Some(exit);
        Produces::ok(())
    }
}

#[async_trait::async_trait]
pub(crate) trait ChildExitHook: Actor {
    async fn child_exit(&mut self, exit: ChildExit) -> ActorResult<()>;
}

#[derive(Clone, Debug)]
pub enum ChildExit {
    Child(CommandConfig, PidExit),
    Unknown(PidExit),
}
impl ChildExit {
    pub fn check(self) -> Result<(), Error> {
        let exit = match &self {
            ChildExit::Child(_, exit) => exit,
            ChildExit::Unknown(exit) => exit,
        };
        if exit.reason == PidExitReason::Code(0) {
            Ok(())
        } else {
            Err(Error::ChildError(self))
        }
    }
}
