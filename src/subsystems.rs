use std::any::{/* Any, */ TypeId};
use std::collections::{HashMap, HashSet};
use std::fmt::Debug;

use act_zero::runtimes::default::spawn_actor;
use act_zero::{call, send, upcast, Actor, ActorResult, Addr, Produces};
use async_trait::async_trait;
use tokio::sync::watch;

use crate::Error;

pub struct SubsystemsBuild {
    system: Addr<System>,
    actors: HashMap<TypeId, Addr<dyn Subsystem>>,
    running_rx: watch::Receiver<usize>,
}
impl SubsystemsBuild {
    pub fn new() -> Self {
        let (running_tx, running_rx) = watch::channel(0);
        let system = spawn_actor(System {
            running: HashSet::new(),
            actors: HashMap::new(),
            running_tx,
        });
        SubsystemsBuild {
            system,
            actors: HashMap::new(),
            running_rx,
        }
    }

    pub(crate) fn spawn_actor<T: Subsystem, F: FnOnce(&mut SubsystemsBuild, Addr<System>) -> T>(
        &mut self,
        init: F,
    ) {
        let key = TypeId::of::<T>();
        if !self.actors.contains_key(&key) {
            let value = init(self, self.system.clone());
            let addr = spawn_actor(value);
            self.actors.insert(key, upcast!(addr));
        }
    }

    pub(crate) fn get<T: Send + 'static>(&self) -> Addr<T> {
        let state = self.actors.get(&TypeId::of::<T>()).unwrap();
        state.clone().downcast().unwrap()
    }

    pub async fn start(self) -> Result<SubsystemsRuntime, Error> {
        let Self {
            system,
            actors,
            running_rx,
        } = self;
        call!(system.start(actors.clone())).await?;
        Ok(SubsystemsRuntime {
            system,
            actors,
            running_rx,
        })
    }
}

#[async_trait]
pub(crate) trait Subsystem: Actor {
    async fn ping(&mut self) -> ActorResult<()> {
        Produces::ok(())
    }

    fn send(&self, system: &Addr<System>, events: Vec<Event>) {
        let id = TypeId::of::<Self>();
        send!(system.subsystem_events(id, events));
    }

    async fn stop(&mut self) -> ActorResult<()>;
}

pub struct SubsystemsRuntime {
    system: Addr<System>,
    actors: HashMap<TypeId, Addr<dyn Subsystem>>,
    running_rx: watch::Receiver<usize>,
}
impl SubsystemsRuntime {
    pub(crate) fn get<T: Send + 'static>(&self) -> Addr<T> {
        let state = self.actors.get(&TypeId::of::<T>()).unwrap();
        state.clone().downcast().unwrap()
    }

    pub async fn wait(&mut self) -> Result<(), Error> {
        loop {
            if *self.running_rx.borrow() == 0 {
                return Ok(());
            }
            self.running_rx.changed().await?;
        }
    }

    pub async fn stop(&self) -> Result<(), Error> {
        send!(self.system.stop());
        Ok(())
    }
}

pub(crate) struct System {
    running: HashSet<TypeId>,
    actors: HashMap<TypeId, Addr<dyn Subsystem>>,
    running_tx: watch::Sender<usize>,
}
impl System {
    async fn start(&mut self, actors: HashMap<TypeId, Addr<dyn Subsystem>>) -> ActorResult<()> {
        for actor in actors.values() {
            call!(actor.ping()).await?;
        }
        self.actors.extend(actors);
        Produces::ok(())
    }
    pub(crate) async fn subsystem_events(
        &mut self,
        id: TypeId,
        events: Vec<Event>,
    ) -> ActorResult<()> {
        use Event::*;
        for event in events {
            match event {
                Idle => {
                    self.running.remove(&id);
                }
                Info(event) => {
                    log::info!("{:?}", event);
                }
                Running => {
                    self.running.insert(id);
                }
                ActorError(error) => {
                    log::error!("{}", error);
                    self.running.remove(&id);
                    self.stop_running();
                }
            };
        }
        self.running_tx.send(self.running.len())?;
        Produces::ok(())
    }
    pub(crate) async fn stop(&mut self) -> ActorResult<()> {
        log::info!("Stopping");
        self.stop_running();
        Produces::ok(())
    }
    fn stop_running(&mut self) {
        for id in &self.running {
            if let Some(actor) = self.actors.get(&id) {
                send!(actor.stop());
            }
        }
    }
}
impl Actor for System {}

pub type Message = Box<dyn Debug + Send + Sync>;

#[derive(Debug)]
pub(crate) enum Event {
    Idle,
    Info(Message),
    Running,
    ActorError(act_zero::ActorError),
}
