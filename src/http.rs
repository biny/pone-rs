use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;

use act_zero::{call, send, Actor, ActorResult, Addr, AddrLike, Produces};
use hyper::{Body, Method, Request, Response, Server};
use routerify::{Router, RouterBuilder, RouterService};

use crate::for_subsystem::*;
use crate::nng_stream::NngSocket;
use crate::serde_http;

type BodyRouterBuilder = RouterBuilder<Body, HandlerError>;
type HandlerResult = Result<Response<Body>, HandlerError>;

async fn handle_static(_req: Request<Body>, value: Arc<String>) -> HandlerResult {
    Ok(Response::new(value.to_string().into()))
}

async fn handle_nng(req: Request<Body>, url: Arc<String>) -> HandlerResult {
    let client = nng::Socket::new(nng::Protocol::Req0)?;
    let client = NngSocket::new(client, Some(Duration::from_secs(60)))?;
    let mut client = client.dial(&url)?;
    let req = serde_http::to_string(req).await?;
    let res = client.send_receive(req.as_bytes()).await?;
    Ok(serde_http::from_slice(&res[..])?)
}

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub enum RouteHandler {
    #[serde(rename = "static")]
    StaticString(String),
    #[serde(rename = "nng")]
    Nng(String),
}
impl RouteHandler {
    fn add_route(
        &self,
        router: BodyRouterBuilder,
        path: &str,
        methods: Vec<Method>,
    ) -> BodyRouterBuilder {
        match self {
            RouteHandler::StaticString(value) => {
                let value = Arc::new(value.to_string());
                router.add(path, methods, move |req| handle_static(req, value.clone()))
            }
            RouteHandler::Nng(url) => {
                let url = Arc::new(url.to_string());
                router.add(path, methods, move |req| handle_nng(req, url.clone()))
            }
        }
    }
}

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub struct RouteDefinition {
    #[serde(rename = "method")]
    methods: Vec<String>,
    path: String,
    handler: RouteHandler,
}

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub struct HttpService {
    listen: String,
    #[serde(rename = "route")]
    routes: Vec<RouteDefinition>,
}
impl HttpService {
    fn add_routes(&self, router: BodyRouterBuilder) -> Result<BodyRouterBuilder, Error> {
        Ok(self
            .routes
            .iter()
            .try_fold(router, |router, definition| -> Result<_, Error> {
                let methods = definition
                    .methods
                    .iter()
                    .map(|m| m.parse())
                    .collect::<Result<Vec<_>, _>>()?;
                Ok(definition
                    .handler
                    .add_route(router, &definition.path, methods))
            })?)
    }
}

pub trait HttpSubsystem {
    fn init_http(&mut self);
}
impl HttpSubsystem for SubsystemsBuild {
    fn init_http(&mut self) {
        self.spawn_actor(|_build, system| Http { system });
    }
}

#[async_trait::async_trait]
pub trait HttpSubsystemRuntime {
    async fn start_http(&self, s: HttpService) -> Result<SocketAddr, Error>;
}
#[async_trait::async_trait]
impl HttpSubsystemRuntime for SubsystemsRuntime {
    async fn start_http(&self, s: HttpService) -> Result<SocketAddr, Error> {
        let router = Router::builder();
        let router = s.add_routes(router)?;
        let router = router.build()?;
        let router = RouterService::new(router)?;

        let addr = s.listen.parse()?;

        let actor: Addr<Http> = self.get();
        call!(actor.server_start(s)).await?;

        let server = Server::bind(&addr).serve(router);
        let addr = server.local_addr();
        let server_actor = actor.clone();
        let server = async move {
            if let Err(error) = server.await {
                send!(server_actor.server_error(error));
            }
        };
        actor.send_fut(server);
        Ok(addr)
    }
}

pub struct Http {
    system: Addr<System>,
}
impl Http {
    async fn server_start(&mut self, s: HttpService) -> ActorResult<()> {
        self.send(&self.system, vec![Event::Info(Box::new(s)), Event::Running]);
        Produces::ok(())
    }
    async fn server_error(&mut self, error: hyper::Error) -> ActorResult<()> {
        Err(error)?;
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Subsystem for Http {
    async fn stop(&mut self) -> ActorResult<()> {
        self.send(&self.system, vec![Event::Idle]);
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Actor for Http {
    async fn error(&mut self, error: act_zero::ActorError) -> bool {
        self.send(&self.system, vec![Event::ActorError(error)]);
        true
    }
}

#[derive(thiserror::Error, Debug)]
enum HandlerError {
    #[error("Failed to send NNG request: {0}")]
    NngError(#[from] nng::Error),
    #[error("Failed to serialize request: {0}")]
    SerdeHttpRequestError(#[from] serde_http::RequestError),
    #[error("Failed to parse response: {0}")]
    SerdeHttpRespnseError(#[from] serde_http::ResponseError),
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;
    use std::future::Future;
    use std::str::from_utf8;
    use std::vec::Vec;

    use hyper::{Client, StatusCode};

    use crate::{cmd, Command, CommandOptions};
    use crate::{NngChildrenService, NngChildrenSubsystem, NngChildrenSubsystemRuntime};

    use super::*;

    async fn with_http<F, Fut>(
        name: &'static str,
        test: F,
    ) -> Result<(), Box<dyn std::error::Error>>
    where
        F: FnOnce(String) -> Fut,
        Fut: Future<Output = Result<(), Box<dyn std::error::Error>>>,
    {
        let mut subsystems = SubsystemsBuild::new();
        subsystems.init_nng_children();
        subsystems.init_http();
        let mut subsystems = subsystems.start().await?;

        let nng_url = format!("inproc://pone/http/{}", name);
        let service = NngChildrenService {
            api: nng_url.to_string(),
            command: cmd!("build/target/debug/test-client"),
        };
        subsystems.start_nng_children(service).await?;

        let service = HttpService {
            listen: "127.0.0.1:0".into(),
            routes: vec![
                RouteDefinition {
                    methods: vec!["GET".into()],
                    path: "/hello".into(),
                    handler: RouteHandler::StaticString("Hello World!".into()),
                },
                RouteDefinition {
                    methods: vec!["GET".into(), "POST".into()],
                    path: "/nng/:id".into(),
                    handler: RouteHandler::Nng(nng_url.into()),
                },
            ],
        };
        let addr = subsystems.start_http(service).await?;
        dbg!("started");

        let test = test(format!("http://127.0.0.1:{}", addr.port())).await;

        dbg!("stopping", &test);
        subsystems.stop().await?;
        subsystems.wait().await?;

        test?;
        Ok(())
    }

    #[tokio::test]
    async fn nothing() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nothing", |_port| async move { Ok(()) }).await?;
        Ok(())
    }

    #[tokio::test]
    async fn not_found() -> Result<(), Box<dyn std::error::Error>> {
        with_http("not_found", |base| async move {
            let uri = format!("{}/not_found", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::NOT_FOUND, res.status());
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[tokio::test]
    async fn hello() -> Result<(), Box<dyn std::error::Error>> {
        with_http("hello", |base| async move {
            let uri = format!("{}/hello", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::OK, res.status());
            assert_eq!(
                "Hello World!",
                from_utf8(hyper::body::to_bytes(res.into_body()).await?.as_ref())?
            );
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[tokio::test]
    async fn nng_hello() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_hello", |base| async move {
            let uri = format!("{}/nng/hello?Nng", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::OK, res.status());
            assert_eq!(
                "Hello Nng!",
                from_utf8(hyper::body::to_bytes(res.into_body()).await?.as_ref())?
            );
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[derive(serde::Deserialize)]
    struct Body {
        id: String,
        path: String,
        headers: HashMap<String, Vec<String>>,
        body: Option<serde_json::Value>,
    }

    #[tokio::test]
    async fn nng_get() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_get", |base| async move {
            let uri = format!("{}/nng/echo", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::OK, res.status());
            let headers = res.headers();
            assert_eq!(
                vec!["etag", "content-type", "content-length", "date"],
                headers.keys().collect::<Vec<_>>()
            );
            assert_eq!(
                vec!["application/json"],
                headers.get_all("content-type").iter().collect::<Vec<_>>()
            );
            let body = hyper::body::to_bytes(res.into_body()).await?;
            let body = from_utf8(body.as_ref())?;
            dbg!(&body);
            let body: Body = serde_json::from_str(body)?;
            assert_eq!("echo", &body.id);
            assert_eq!("/nng/echo", &body.path);
            assert_eq!(vec!["host"], body.headers.keys().collect::<Vec<_>>());
            assert_eq!(&None, &body.body);
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[tokio::test]
    async fn nng_post() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_post", |base| async move {
            let uri = format!("{}/nng/echo", base);
            let req = Request::builder()
                .method(Method::POST)
                .uri(uri)
                .header("content-type", "application/json")
                .body(hyper::Body::from("{}"))?;
            let res = Client::new().request(req).await?;
            assert_eq!(StatusCode::OK, res.status());
            let headers = res.headers();
            assert_eq!(
                vec!["etag", "content-type", "content-length", "date"],
                headers.keys().collect::<Vec<_>>()
            );
            assert_eq!(
                vec!["application/json"],
                headers.get_all("content-type").iter().collect::<Vec<_>>()
            );
            let body = hyper::body::to_bytes(res.into_body()).await?;
            let body = from_utf8(body.as_ref())?;
            dbg!(&body);
            let body: Body = serde_json::from_str(body)?;
            assert_eq!("echo", &body.id);
            assert_eq!("/nng/echo", &body.path);
            let mut headers = body.headers.keys().collect::<Vec<_>>();
            headers.sort();
            assert_eq!(vec!["content-length", "content-type", "host"], headers);
            assert_eq!(&Some(serde_json::json!({})), &body.body);
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[tokio::test]
    async fn nng_not_found() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_not_found", |base| async move {
            let uri = format!("{}/nng/not_found", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::NOT_FOUND, res.status());
            let headers = res.headers();
            assert_eq!(
                vec!["content-type", "content-length", "date"],
                headers.keys().collect::<Vec<_>>()
            );
            assert_eq!(
                vec!["application/json"],
                headers.get_all("content-type").iter().collect::<Vec<_>>()
            );
            let body = hyper::body::to_bytes(res.into_body()).await?;
            let body = from_utf8(body.as_ref())?;
            assert_eq!("", body);
            Ok(())
        })
        .await?;
        Ok(())
    }

    #[tokio::test]
    async fn nng_invalid_response() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_invalid_response", |base| async move {
            let uri = format!("{}/nng/invalid_response", base).parse()?;
            let res = Client::new().get(uri).await?;
            assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, res.status());
            let headers = res.headers();
            assert_eq!(vec!["content-type", "content-length", "date"], headers.keys().collect::<Vec<_>>());
            assert_eq!(vec!["text/plain"], headers.get_all("content-type").iter().collect::<Vec<_>>());
            let body = hyper::body::to_bytes(res.into_body()).await?;
            let body = from_utf8(body.as_ref())?;
            assert_eq!("Internal Server Error: Failed to parse response: Failed to parse JSON: expected value at line 1 column 1: invalid response", body);
            Ok(())
        }).await?;
        Ok(())
    }

    #[tokio::test]
    async fn nng_post_binary() -> Result<(), Box<dyn std::error::Error>> {
        with_http("nng_post_binary", |base| async move {
            let data = vec![0u8, 1u8, 2u8, 3u8, 4u8];
            let uri = format!("{}/nng/body-echo", base);
            let req = Request::builder()
                .method(Method::POST)
                .uri(uri)
                .header("content-type", "application/binary")
                .body(hyper::Body::from(data.clone()))?;
            let res = Client::new().request(req).await?;
            assert_eq!(StatusCode::OK, res.status());
            let headers = res.headers();
            assert_eq!(
                vec!["content-type", "content-length", "date"],
                headers.keys().collect::<Vec<_>>()
            );
            assert_eq!(
                vec!["application/binary"],
                headers.get_all("content-type").iter().collect::<Vec<_>>()
            );
            let body = hyper::body::to_bytes(res.into_body()).await?;
            assert_eq!(body, data);
            Ok(())
        })
        .await?;
        Ok(())
    }
}
