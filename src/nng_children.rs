use act_zero::{call, upcast, Actor, ActorResult, Addr, Produces};

use crate::for_subsystem::*;
use crate::nng_stream::{NngForward, NngForwardOwner};
use crate::{Children, ChildrenSubsystem, CommandConfig};

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub struct NngChildrenService {
    pub api: String,
    pub command: super::Command,
}

pub trait NngChildrenSubsystem {
    fn init_nng_children(&mut self);
}
impl NngChildrenSubsystem for SubsystemsBuild {
    fn init_nng_children(&mut self) {
        self.init_children();
        let children = self.get();
        self.spawn_actor(|_build, system| NngChildren {
            system,
            children,
            tempdir: tempfile::tempdir().unwrap(),
            socket_count: 0,
            forward: None,
        });
    }
}

#[async_trait::async_trait]
pub trait NngChildrenSubsystemRuntime {
    async fn start_nng_children(&self, service: NngChildrenService) -> Result<(), Error>;
}
#[async_trait::async_trait]
impl NngChildrenSubsystemRuntime for SubsystemsRuntime {
    async fn start_nng_children(&self, s: NngChildrenService) -> Result<(), Error> {
        let actor: Addr<NngChildren> = self.get();
        Ok(call!(actor.start_nng_children(s)).await?)
    }
}

struct NngChildren {
    system: Addr<System>,
    children: Addr<Children>,
    tempdir: tempfile::TempDir,
    socket_count: u16,
    forward: Option<NngForward<CommandConfig>>,
}
impl NngChildren {
    async fn start_nng_children(&mut self, s: NngChildrenService) -> ActorResult<()> {
        self.send(
            &self.system,
            vec![Event::Info(Box::new(s.clone())), Event::Running],
        );
        self.socket_count += 1;
        let index = self.socket_count;
        let worker = self.tempdir.path().join(format!("{}.sock", index));
        let worker = format!("ipc://{}", worker.to_str().unwrap());
        let mut command: CommandConfig = s.command.into();
        command.env("PONE_NNG_WORKER_URL", &worker);
        self.forward.as_ref().unwrap().forward_to_on_demand_worker(
            &s.api,
            &worker.clone(),
            &command,
        )?;
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Subsystem for NngChildren {
    async fn stop(&mut self) -> ActorResult<()> {
        self.send(&self.system, vec![Event::Idle]);
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Actor for NngChildren {
    async fn started(&mut self, addr: Addr<Self>) -> ActorResult<()>
    where
        Self: Sized,
    {
        self.forward.replace(NngForward::new(upcast!(addr)));
        Produces::ok(())
    }

    async fn error(&mut self, error: act_zero::ActorError) -> bool {
        self.send(&self.system, vec![Event::ActorError(error)]);
        true
    }
}
#[async_trait::async_trait]
impl NngForwardOwner<CommandConfig> for NngChildren {
    async fn nng_forward_pipe(&mut self, command: CommandConfig, pipes: usize) -> ActorResult<()> {
        call!(self
            .children
            .start_limited(std::cmp::max(pipes, 2), command))
        .await??;
        Produces::ok(())
    }

    async fn nng_forward_exit(&mut self, result: nng::Result<()>) -> ActorResult<()> {
        result?;
        Produces::ok(())
    }
}

#[cfg(test)]
mod test {
    use std::future::Future;
    use std::time::Duration;

    use crate::for_main::*;
    use crate::nng_stream::NngSocket;
    use crate::{cmd, Command, CommandOptions};

    use super::*;

    async fn with_nng<F, Fut>(executable: &'static str, test: F) -> Result<(), Error>
    where
        F: FnOnce(String) -> Fut,
        Fut: Future<Output = Result<(), Error>>,
    {
        let name = executable.replace("/", "-");
        let api = format!("inproc://pone/nng_children/{}", name);

        let mut subsystems = SubsystemsBuild::new();
        subsystems.init_nng_children();
        let mut subsystems = subsystems.start().await?;

        let service = NngChildrenService {
            api: api.clone(),
            command: cmd!(executable),
        };
        subsystems.start_nng_children(service).await?;

        let test = test(api).await;

        subsystems.stop().await?;
        subsystems.wait().await?;

        test?;
        Ok(())
    }

    #[tokio::test]
    async fn no_client() -> Result<(), Box<dyn std::error::Error>> {
        with_nng("no_client", |_url| async move { Ok(()) }).await?;
        Ok(())
    }

    #[tokio::test]
    async fn command_false() -> Result<(), Box<dyn std::error::Error>> {
        let r = with_nng("command_false", |url| async move {
            let client = nng::Socket::new(nng::Protocol::Req0)?;
            let client = NngSocket::new(client, Some(Duration::from_secs(1)))?;
            let mut client = client.dial(&url)?;
            let msg = client.send_receive("ping".as_bytes()).await?;
            assert_eq!("pong", String::from_utf8_lossy(&msg[..]));
            Ok(())
        })
        .await;
        assert_eq!("NngError(TimedOut)", format!("{:?}", r.unwrap_err()));
        Ok(())
    }

    #[tokio::test]
    async fn ping_pong() -> Result<(), Box<dyn std::error::Error>> {
        with_nng("build/target/debug/test-client", |url| async move {
            let client = nng::Socket::new(nng::Protocol::Req0)?;
            let client = NngSocket::new(client, Some(Duration::from_secs(1)))?;
            let mut client = client.dial(&url)?;
            let msg = client.send_receive("ping".as_bytes()).await?;
            assert_eq!("pong", String::from_utf8_lossy(&msg[..]));
            Ok(())
        })
        .await?;
        Ok(())
    }
}
