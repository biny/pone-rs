use crate::for_subsystem::*;
use crate::{Children, ChildrenSubsystem};
use act_zero::runtimes::default::Timer;
use act_zero::timer::Tick;
use act_zero::{call, send, Actor, ActorResult, Addr, Produces, WeakAddr};
use chrono::{DateTime, Local};
use cron::Schedule;
use std::str::FromStr;

struct CronScheduler<T> {
    schedules: Vec<Schedule>,
    items: Vec<T>,
}
impl<T> CronScheduler<T> {
    fn new() -> Self {
        Self {
            schedules: Vec::new(),
            items: Vec::new(),
        }
    }
    fn is_empty(&self) -> bool {
        self.schedules.is_empty()
    }
    fn add(&mut self, expression: &str, item: T) -> Result<(), Error> {
        let schedule = Schedule::from_str(expression)?;
        self.schedules.push(schedule);
        self.items.push(item);
        Ok(())
    }
    fn after(&self, after: &DateTime<Local>) -> Option<(DateTime<Local>, Vec<&T>)> {
        self.schedules
            .iter()
            .enumerate()
            .filter_map(|(index, schedule)| {
                schedule
                    .after(after)
                    .next()
                    .map(|dt| (dt, &self.items[index]))
            })
            .fold(None, |result, (dt, item)| {
                Some(match result {
                    None => (dt, vec![item]),
                    Some((min, _items)) if dt < min => (dt, vec![item]),
                    Some((min, mut items)) if dt == min => {
                        items.push(item);
                        (dt, items)
                    }
                    Some((min, items)) => (min, items),
                })
            })
    }
}

#[derive(serde::Deserialize, PartialEq, Clone, Debug)]
pub struct CronService {
    pub schedule: String,
    pub command: super::Command,
}

pub trait CronSubsystem {
    fn init_cron(&mut self);
}
impl CronSubsystem for SubsystemsBuild {
    fn init_cron(&mut self) {
        self.init_children();
        let children = self.get();
        self.spawn_actor(|_build, system| Cron {
            system,
            addr: WeakAddr::detached(),
            children,
            scheduler: CronScheduler::new(),
            now: Local::now(),
            timer: Timer::default(),
        });
    }
}

#[async_trait::async_trait]
pub trait CronSubsystemRuntime {
    async fn start_cron(&self, s: CronService) -> Result<(), Error>;
}
#[async_trait::async_trait]
impl CronSubsystemRuntime for SubsystemsRuntime {
    async fn start_cron(&self, s: CronService) -> Result<(), Error> {
        let actor: Addr<Cron> = self.get();
        Ok(call!(actor.schedule(s)).await?)
    }
}

struct Cron {
    system: Addr<System>,
    addr: WeakAddr<Self>,
    children: Addr<Children>,
    scheduler: CronScheduler<super::Command>,
    now: DateTime<Local>,
    timer: Timer,
}
impl Cron {
    fn setup_timer(&mut self) {
        if let Some((dt, _commands)) = self.scheduler.after(&self.now) {
            let duration = (dt - Local::now())
                .to_std()
                .unwrap_or(std::time::Duration::ZERO);
            self.timer.set_timeout_for_weak(self.addr.clone(), duration);
        }
    }
    fn execute(&mut self) {
        if let Some((dt, commands)) = self.scheduler.after(&self.now) {
            if dt > Local::now() {
                return;
            }
            let commands = commands.into_iter().cloned().collect::<Vec<_>>();
            self.now = dt;
            for command in commands {
                send!(self.children.start_simple(command));
            }
        }
    }

    async fn schedule(&mut self, s: CronService) -> ActorResult<()> {
        self.send(
            &self.system,
            vec![Event::Info(Box::new(s.clone())), Event::Running],
        );
        if self.scheduler.is_empty() {
            self.now = Local::now()
        }
        self.scheduler.add(&s.schedule, s.command.clone())?;
        self.setup_timer();
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Subsystem for Cron {
    async fn stop(&mut self) -> ActorResult<()> {
        self.timer.clear();
        self.send(&self.system, vec![Event::Idle]);
        Produces::ok(())
    }
}
#[async_trait::async_trait]
impl Actor for Cron {
    async fn started(&mut self, addr: Addr<Self>) -> ActorResult<()> {
        self.addr = addr.downgrade();
        Produces::ok(())
    }

    async fn error(&mut self, error: act_zero::ActorError) -> bool {
        self.send(&self.system, vec![Event::ActorError(error)]);
        true
    }
}
#[async_trait::async_trait]
impl Tick for Cron {
    async fn tick(&mut self) -> ActorResult<()> {
        self.execute();
        self.setup_timer();
        Produces::ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::TimeZone;

    #[tokio::test]
    async fn scheduler_next() -> Result<(), Box<dyn std::error::Error>> {
        let mut scheduler = CronScheduler::new();
        scheduler.add("1 1 * * * * *", 1)?;
        scheduler.add("1 1 1 * * * *", 2)?;
        scheduler.add("1 1 2 * * * *", 3)?;
        assert_eq!(
            scheduler.after(&Local.with_ymd_and_hms(2022, 5, 14, 0, 0, 0).unwrap()),
            Some((
                Local.with_ymd_and_hms(2022, 5, 14, 0, 1, 1).unwrap(),
                vec![&1]
            ))
        );
        assert_eq!(
            scheduler.after(&Local.with_ymd_and_hms(2022, 5, 14, 1, 0, 0).unwrap()),
            Some((
                Local.with_ymd_and_hms(2022, 5, 14, 1, 1, 1).unwrap(),
                vec![&1, &2]
            ))
        );
        assert_eq!(
            scheduler.after(&Local.with_ymd_and_hms(2022, 5, 14, 2, 0, 0).unwrap()),
            Some((
                Local.with_ymd_and_hms(2022, 5, 14, 2, 1, 1).unwrap(),
                vec![&1, &3]
            ))
        );
        Ok(())
    }
}
