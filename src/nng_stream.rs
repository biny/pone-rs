// use std::cell::Cell;
use std::pin::Pin;
// use std::rc::Rc;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc, Mutex,
};
use std::task::Poll;
use std::time::Duration;

use act_zero::{send, Actor, ActorResult, Addr};
// use futures::channel::mpsc;
use futures::prelude::*;
use nng::options::Options;

pub(crate) struct NngSocket {
    socket: nng::Socket,
    recv_after_send: bool,
    sink_aio: nng::Aio,
    sink_mutex: Arc<Mutex<State<nng::AioResult>>>,
    stream_aio: nng::Aio,
    stream_mutex: Arc<Mutex<State<nng::AioResult>>>,
}
#[allow(dead_code)]
impl NngSocket {
    pub(crate) fn new(socket: nng::Socket, timeout: Option<Duration>) -> nng::Result<Self> {
        socket.set_opt::<nng::options::RecvTimeout>(timeout)?;
        socket.set_opt::<nng::options::SendTimeout>(timeout)?;
        let (sink_aio, sink_mutex) = Self::new_aio()?;
        let (stream_aio, stream_mutex) = Self::new_aio()?;
        Ok(Self {
            socket,
            recv_after_send: false,
            sink_aio,
            sink_mutex,
            stream_aio,
            stream_mutex,
        })
    }

    fn new_aio() -> nng::Result<(nng::Aio, Arc<Mutex<State<nng::AioResult>>>)> {
        let mutex = Arc::new(Mutex::new(State::Idle));
        let aio_mutex = mutex.clone();
        let aio = nng::Aio::new(move |_, result| {
            let mut state = aio_mutex.lock().unwrap();
            match std::mem::replace(&mut *state, State::Ready(result)) {
                State::Idle | State::Requested => {}
                State::Waiting(waker) => waker.wake(),
                State::Ready(_) => unimplemented!(),
            }
        })?;
        Ok((aio, mutex))
    }

    pub(crate) fn pipe_notify(&self) -> nng::Result<NngPipeEventStream> {
        NngPipeEventStream::new(&self.socket)
    }

    pub(crate) fn dial(self, url: &str) -> nng::Result<Self> {
        self.socket.dial_async(url)?;
        Ok(self)
    }

    pub(crate) fn listen(self, url: &str) -> nng::Result<Self> {
        self.socket.listen(url)?;
        Ok(self)
    }

    pub(crate) fn set_recv_after_send(mut self) -> nng::Result<Self> {
        self.recv_after_send = true;
        self.socket.recv_async(&self.stream_aio)?;
        Ok(self)
    }

    pub(crate) fn recv(&self) -> nng::Result<()> {
        self.socket.recv_async(&self.stream_aio)?;
        Ok(())
    }

    pub(crate) async fn send_receive<M: Into<nng::Message>>(
        &mut self,
        msg: M,
    ) -> nng::Result<nng::Message> {
        self.send(msg.into()).await?;
        self.next().await.ok_or(nng::Error::Closed)?
    }
}
impl Sink<nng::Message> for NngSocket {
    type Error = nng::Error;

    fn poll_ready(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context,
    ) -> Poll<Result<(), Self::Error>> {
        self.poll_flush(cx)
    }

    fn start_send(self: Pin<&mut Self>, msg: nng::Message) -> Result<(), Self::Error> {
        let mut state = self.sink_mutex.lock().unwrap();
        *state = State::Requested;
        self.socket.send_async(&self.sink_aio, msg)?;
        Ok(())
    }

    fn poll_flush(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context,
    ) -> Poll<Result<(), Self::Error>> {
        let mut state = self.sink_mutex.lock().unwrap();
        match std::mem::replace(&mut *state, State::Idle) {
            State::Idle => Poll::Ready(Ok(())),
            State::Requested | State::Waiting(_) => {
                *state = State::Waiting(cx.waker().clone());
                Poll::Pending
            }
            State::Ready(nng::AioResult::Send(result)) => {
                if self.recv_after_send {
                    self.socket.recv_async(&self.stream_aio)?;
                }
                Poll::Ready(result.map_err(|(_, error)| error))
            }
            State::Ready(nng::AioResult::Recv(_)) | State::Ready(nng::AioResult::Sleep(_)) => {
                unimplemented!()
            }
        }
    }

    fn poll_close(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context,
    ) -> Poll<Result<(), Self::Error>> {
        self.poll_flush(cx)
    }
}
impl Stream for NngSocket {
    type Item = Result<nng::Message, nng::Error>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut std::task::Context) -> Poll<Option<Self::Item>> {
        let mut state = self.stream_mutex.lock().unwrap();
        match std::mem::replace(&mut *state, State::Idle) {
            State::Idle | State::Requested => {
                if !self.recv_after_send {
                    self.socket.recv_async(&self.stream_aio)?;
                }
                *state = State::Waiting(cx.waker().clone());
                Poll::Pending
            }
            State::Waiting(_) => {
                *state = State::Waiting(cx.waker().clone());
                Poll::Pending
            }
            State::Ready(nng::AioResult::Recv(result)) => Poll::Ready(Some(result)),
            State::Ready(nng::AioResult::Send(_)) | State::Ready(nng::AioResult::Sleep(_)) => {
                unimplemented!()
            }
        }
    }
}

pub(crate) struct NngPipeEventStream {
    mutex: Arc<Mutex<State<Vec<nng::PipeEvent>>>>,
}
impl NngPipeEventStream {
    fn new(socket: &nng::Socket) -> nng::Result<Self> {
        let mutex: Arc<Mutex<State<Vec<nng::PipeEvent>>>> = Arc::new(Mutex::new(State::Idle));
        let notify_mutex = mutex.clone();
        socket.pipe_notify(move |_pipe, event| {
            let mut state = notify_mutex.lock().unwrap();
            let events = match std::mem::replace(&mut *state, State::Idle) {
                State::Idle | State::Requested => vec![event],
                State::Waiting(waker) => {
                    waker.wake();
                    vec![event]
                }
                State::Ready(mut events) => {
                    events.push(event);
                    events
                }
            };
            *state = State::Ready(events);
        })?;
        Ok(Self { mutex })
    }
}
impl Stream for NngPipeEventStream {
    type Item = nng::PipeEvent;

    fn poll_next(self: Pin<&mut Self>, cx: &mut std::task::Context) -> Poll<Option<Self::Item>> {
        let mut state = self.mutex.lock().unwrap();
        match std::mem::replace(&mut *state, State::Idle) {
            State::Idle | State::Requested | State::Waiting(_) => {
                *state = State::Waiting(cx.waker().clone());
                Poll::Pending
            }
            State::Ready(mut events) => {
                let result = Poll::Ready(Some(events.remove(0)));
                if !events.is_empty() {
                    *state = State::Ready(events);
                }
                result
            }
        }
    }
}

pub(crate) struct NngForward<T: 'static> {
    owner: Addr<dyn NngForwardOwner<T>>,
    pipes: Arc<AtomicUsize>,
}
impl<T: Clone + Send + Sync> NngForward<T> {
    pub(crate) fn new(owner: Addr<dyn NngForwardOwner<T>>) -> Self {
        Self {
            owner,
            pipes: Arc::new(AtomicUsize::new(0)),
        }
    }

    pub(crate) fn forward(&self, s1: nng::RawSocket, s2: nng::RawSocket) {
        let owner = self.owner.clone();
        std::thread::spawn(move || {
            let result = nng::forwarder(s1, s2);
            send!(owner.nng_forward_exit(result));
        });
    }

    pub(crate) fn forward_to_on_demand_worker(
        &self,
        rep: &str,
        req: &str,
        value: &T,
    ) -> nng::Result<()> {
        let value = value.clone();
        let owner = self.owner.clone();
        let pipes = self.pipes.clone();
        let reps = nng::RawSocket::new(nng::Protocol::Rep0)?;
        let reqs = nng::RawSocket::new(nng::Protocol::Req0)?;
        reps.socket.pipe_notify(move |_pipe, event| {
            match event {
                nng::PipeEvent::AddPre => {
                    send!(owner.nng_forward_pipe(value.clone(), pipes.load(Ordering::SeqCst) + 1));
                }
                nng::PipeEvent::AddPost => {
                    pipes.fetch_add(1, Ordering::SeqCst);
                }
                nng::PipeEvent::RemovePost => {
                    pipes.fetch_sub(1, Ordering::SeqCst);
                }
                _ => {}
            };
        })?;
        reps.socket.listen(rep)?;
        reqs.socket.listen(req)?;
        self.forward(reps, reqs);
        Ok(())
    }
}

#[async_trait::async_trait]
pub(crate) trait NngForwardOwner<T>: Actor {
    async fn nng_forward_pipe(&mut self, value: T, pipes: usize) -> ActorResult<()>;
    async fn nng_forward_exit(&mut self, result: nng::Result<()>) -> ActorResult<()>;
}

enum State<T> {
    Idle,
    Requested,
    Waiting(std::task::Waker),
    Ready(T),
}

#[cfg(test)]
mod test {
    use std::io::Write;

    use act_zero::runtimes::default::spawn_actor;
    use act_zero::{upcast, AddrLike, Produces, WeakAddr};

    use super::*;

    const API: &str = "inproc://pone/nng_stream/api";
    const WORK: &str = "inproc://pone/nng_stream/work";

    fn start(url: &str) -> nng::Result<NngSocket> {
        Ok(NngSocket::new(
            nng::Socket::new(nng::Protocol::Rep0)?,
            Some(Duration::from_secs(10)),
        )?
        .dial(url)?
        .set_recv_after_send()?)
    }

    struct Owner {
        servers: u8,
        addr: WeakAddr<Self>,
    }
    #[async_trait::async_trait]
    impl NngForwardOwner<String> for Owner {
        async fn nng_forward_pipe(&mut self, value: String, _pipes: usize) -> ActorResult<()> {
            if self.servers > 0 {
                return Produces::ok(());
            }
            self.servers += 1;

            let (sink, stream) = start(&value)?.split();
            let fut = async move {
                stream
                    .map_ok(move |mut msg| {
                        let name = String::from_utf8_lossy(&msg).into_owned();
                        println!("req: {:?}", &name);
                        msg.clear();
                        write!(msg, "Hello {}!", name).unwrap();
                        msg.clone()
                    })
                    .forward(sink)
                    .await
                    .unwrap();
            };
            self.addr.send_fut(fut);
            Produces::ok(())
        }
        async fn nng_forward_exit(&mut self, result: nng::Result<()>) -> ActorResult<()> {
            result?;
            Produces::ok(())
        }
    }
    #[async_trait::async_trait]
    impl Actor for Owner {
        async fn started(&mut self, addr: Addr<Self>) -> ActorResult<()>
        where
            Self: Sized,
        {
            self.addr = addr.downgrade();
            Produces::ok(())
        }
    }

    #[tokio::test]
    async fn test_forward_to_on_demand_worker() -> Result<(), Box<dyn std::error::Error>> {
        let owner = spawn_actor(Owner {
            servers: 0,
            addr: WeakAddr::detached(),
        });
        let forward = NngForward::new(upcast!(owner));
        forward.forward_to_on_demand_worker(API, WORK, &WORK.to_string())?;

        let mut c1 = NngSocket::new(
            nng::Socket::new(nng::Protocol::Req0)?,
            Some(Duration::from_secs(10)),
        )?
        .dial(API)?;
        let mut c2 = NngSocket::new(
            nng::Socket::new(nng::Protocol::Req0)?,
            Some(Duration::from_secs(10)),
        )?
        .dial(API)?;
        let c1 = c1.send_receive("World 1".as_bytes());
        let c2 = c2.send_receive("World 2".as_bytes());
        let (m1, m2) = futures::try_join!(c1, c2)?;
        assert_eq!(
            ("Hello World 1!".to_string(), "Hello World 2!".to_string()),
            (
                String::from_utf8_lossy(&m1).to_string(),
                String::from_utf8_lossy(&m2).to_string()
            )
        );

        Ok(())
    }
}
