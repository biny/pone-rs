use std::collections::HashMap;
use std::vec::Vec;

#[derive(serde::Deserialize)]
struct Request {
    path: String,
    query: Option<String>,
    params: Params,
    headers: HashMap<String, Vec<String>>,
    body: Option<serde_json::Value>,
}
#[derive(serde::Deserialize)]
struct Params {
    id: Option<String>,
}
#[derive(serde::Serialize)]
struct Response {
    status_code: u16,
    headers: HashMap<&'static str, Vec<String>>,
    body: Option<Body>,
}
#[derive(serde::Serialize)]
#[serde(untagged)]
enum Body {
    Hello(String),
    Echo {
        id: Option<String>,
        path: String,
        headers: HashMap<String, Vec<String>>,
        body: Option<serde_json::Value>,
    },
    Value(Option<serde_json::Value>),
}

fn handle_http(req: Request, msg: &mut nng::Message) -> Result<(), Box<dyn std::error::Error>> {
    let Request {
        path,
        query,
        params: Params { id },
        mut headers,
        body,
    } = req;
    let res = match id.as_ref().map(String::as_str) {
        Some("hello") => {
            let body = Some(Body::Hello(format!(
                "Hello {}!",
                query.unwrap_or("World".to_string())
            )));
            serde_json::to_vec(&Response {
                status_code: 200,
                headers: HashMap::new(),
                body,
            })?
        }
        Some("echo") => {
            let body = Some(Body::Echo {
                id,
                path,
                headers,
                body,
            });
            let mut response = Response {
                status_code: 200,
                headers: HashMap::new(),
                body,
            };
            response.headers.insert("ETag", vec!["1".to_string()]);
            serde_json::to_vec(&response)?
        }
        Some("body-echo") => {
            let ctype = headers.remove("content-type").unwrap_or(vec![]);
            let body = Some(Body::Value(body));
            let mut response = Response {
                status_code: 200,
                headers: HashMap::new(),
                body,
            };
            response.headers.insert("Content-Type", ctype);
            serde_json::to_vec(&response)?
        }
        Some("invalid_response") => {
            msg.clear();
            msg.push_front("invalid response".as_bytes());
            return Ok(());
        }
        Some("crash") => std::process::exit(1),
        _ => {
            let response = Response {
                status_code: 404,
                headers: HashMap::new(),
                body: None,
            };
            serde_json::to_vec(&response)?
        }
    };
    msg.clear();
    msg.push_front(&res);
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let url = std::env::var("PONE_NNG_WORKER_URL")?;
    let socket = nng::Socket::new(nng::Protocol::Rep0)?;
    socket.dial(&url)?;
    loop {
        let mut msg = socket.recv()?;
        match &msg[..] {
            b"ping" => {
                msg.clear();
                msg.push_back(b"pong");
            }
            _ => match serde_json::from_slice(&msg[..]) {
                Ok(req) => {
                    handle_http(req, &mut msg)?;
                }
                Err(err) => {
                    let err = format!("ERROR: {:?}", err);
                    msg.clear();
                    msg.push_front(err.as_bytes());
                }
            },
        };
        socket.send(msg).map_err(|(_msg, err)| err)?;
    }
}
