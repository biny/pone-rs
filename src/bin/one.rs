use pone::for_main::*;
use pone::{CommandConfig, CoreSubsystem, CoreSubsystemRuntime};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    simple_logger::SimpleLogger::new().init()?;

    let command: pone::Command = pone::serde_args::from_string_iter(&mut std::env::args().skip(1))?;
    let command: CommandConfig = command.into();

    let mut subsystems = SubsystemsBuild::new();
    subsystems.init_core();

    let runtime = subsystems.start().await?;

    log::info!("Starting {:?}", &command);
    let child = Into::<std::process::Command>::into(&command).spawn()?;

    runtime.waitpid(child.id()).await?;
    Ok(())
}
