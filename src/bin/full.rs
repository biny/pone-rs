use pone::for_main::*;
use pone::{ChildrenSubsystem, ChildrenSubsystemRuntime};
use pone::{CronSubsystem, CronSubsystemRuntime};
use pone::{HttpSubsystem, HttpSubsystemRuntime};
use pone::{NngChildrenSubsystem, NngChildrenSubsystemRuntime};

#[derive(serde::Deserialize, Default, Debug)]
#[serde(default)]
struct Options {
    init: Vec<pone::Command>,
    simple: Vec<pone::Command>,
    nng: Vec<pone::NngChildrenService>,
    http: Vec<pone::HttpService>,
    cron: Vec<pone::CronService>,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    simple_logger::SimpleLogger::new().init()?;

    let options: Options = pone::serde_args::from_string_iter(&mut std::env::args().skip(1))?;

    let mut subsystems = SubsystemsBuild::new();
    subsystems.init_children();
    subsystems.init_nng_children();
    subsystems.init_http();
    subsystems.init_cron();

    let mut subsystems = subsystems.start().await?;

    log::info!("Starting init");
    for command in options.init {
        subsystems.start_simple(command).await?;
        subsystems.wait().await?;
        subsystems.last_exit().await?.unwrap().check()?;
    }

    log::info!("Starting services");
    for nng in options.nng {
        subsystems.start_nng_children(nng).await?;
    }
    for http in options.http {
        subsystems.start_http(http).await?;
    }
    for cron in options.cron {
        subsystems.start_cron(cron).await?;
    }
    for command in options.simple {
        subsystems.start_simple(command).await?;
    }
    subsystems.wait().await?;

    Ok(())
}
