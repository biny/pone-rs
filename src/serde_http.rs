use std::collections::HashMap;
use std::vec::Vec;

use base64::prelude::*;
use hyper::header::{HeaderName, CONTENT_TYPE};
use routerify::prelude::*;

#[derive(serde::Serialize, serde::Deserialize)]
struct Request {
    method: String,
    path: String,
    query: Option<String>,
    params: HashMap<String, String>,
    headers: HashMap<String, Vec<String>>,
    body: Option<serde_json::Value>,
}
impl Request {
    pub(crate) async fn new(req: hyper::Request<hyper::Body>) -> Result<Self, RequestError> {
        let params = req
            .params()
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect();
        let (parts, body) = req.into_parts();
        let headermap = parts.headers;
        let mut headers = HashMap::new();
        for key in headermap.keys() {
            headers.insert(
                key.to_string(),
                headermap
                    .get_all(key)
                    .iter()
                    .filter_map(|value| {
                        if let Ok(value) = value.to_str() {
                            Some(value.to_string())
                        } else {
                            None
                        }
                    })
                    .collect(),
            );
        }
        let body = match headermap.get("content-type") {
            Some(content_type) => {
                let mime: mime::Mime = content_type.to_str()?.parse()?;
                match (mime.type_(), mime.subtype()) {
                    (mime::APPLICATION, mime::JSON) => {
                        let body = hyper::body::to_bytes(body).await?;
                        let body = std::str::from_utf8(body.as_ref())?;
                        dbg!(&body);
                        Some(serde_json::from_str(body)?)
                    }
                    (mime::TEXT, _) => {
                        let body = hyper::body::to_bytes(body).await?;
                        Some(serde_json::Value::String(
                            std::str::from_utf8(body.as_ref())?.to_string(),
                        ))
                    }
                    _ => {
                        let body = hyper::body::to_bytes(body).await?;
                        let body = BASE64_STANDARD.encode(body);
                        Some(serde_json::Value::String(
                            std::str::from_utf8(body.as_ref())?.to_string(),
                        ))
                    }
                }
            }
            None => None,
        };
        Ok(Self {
            method: parts.method.to_string(),
            path: parts.uri.path().to_string(),
            query: parts.uri.query().map(str::to_string),
            params,
            headers,
            body,
        })
    }
}
pub(crate) async fn to_string(req: hyper::Request<hyper::Body>) -> Result<String, RequestError> {
    let req = Request::new(req).await?;
    Ok(serde_json::to_string(&req)?)
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum RequestError {
    #[error(transparent)]
    HyperError(#[from] hyper::Error),
    #[error(transparent)]
    HyperToStrError(#[from] hyper::header::ToStrError),
    #[error(transparent)]
    JsonError(#[from] serde_json::Error),
    #[error(transparent)]
    MimeParseError(#[from] mime::FromStrError),
    #[error(transparent)]
    Utf8Error(#[from] std::str::Utf8Error),
}

#[derive(serde::Serialize, serde::Deserialize)]
struct Response {
    status_code: u16,
    headers: HashMap<String, Vec<String>>,
    body: serde_json::Value,
}
pub(crate) fn from_slice(slice: &[u8]) -> Result<hyper::Response<hyper::Body>, ResponseError> {
    let data: Response = serde_json::from_slice(slice).map_err(|err| {
        ResponseError::JsonParseError(err, String::from_utf8_lossy(slice).to_string())
    })?;
    let mut res = hyper::Response::builder().status(hyper::StatusCode::from_u16(data.status_code)?);

    let content_type = if let Some(headers) = res.headers_mut() {
        for (key, values) in &data.headers {
            for value in values {
                let name: HeaderName = key.parse()?;
                headers.insert(name, value.parse()?);
            }
        }
        match headers.get(CONTENT_TYPE) {
            Some(content_type) => content_type.to_str()?,
            None => {
                let content_type = match &data.body {
                    serde_json::Value::String(_) => "text/plain; charset=utf-8",
                    _ => "application/json",
                };
                headers.insert(CONTENT_TYPE, content_type.parse()?);
                content_type
            }
        }
    } else {
        "application/json"
    };

    let body = match &data.body {
        serde_json::Value::Null => "".as_bytes().into(),
        serde_json::Value::String(value) => {
            let mime: mime::Mime = content_type.parse()?;
            match (mime.type_(), mime.subtype()) {
                (mime::APPLICATION, mime::JSON) => serde_json::to_vec(value)?,
                (mime::TEXT, _) => value.as_bytes().into(),
                _ => BASE64_STANDARD.decode(value)?,
            }
        }
        _ => serde_json::to_vec(&data.body)?,
    };

    Ok(res.body(body.into())?)
}

#[derive(thiserror::Error, Debug)]
pub enum ResponseError {
    #[error(transparent)]
    HttpError(#[from] hyper::http::Error),
    #[error(transparent)]
    InvalidStatusCode(#[from] hyper::http::status::InvalidStatusCode),
    #[error(transparent)]
    InvalidHeaderName(#[from] hyper::header::InvalidHeaderName),
    #[error(transparent)]
    InvalidHeaderValue(#[from] hyper::header::InvalidHeaderValue),
    #[error(transparent)]
    ToStrError(#[from] hyper::header::ToStrError),
    #[error(transparent)]
    MimeParseError(#[from] mime::FromStrError),
    #[error("Failed to parse JSON: {0}: {1}")]
    JsonParseError(serde_json::Error, String),
    #[error(transparent)]
    JsonError(#[from] serde_json::Error),
    #[error(transparent)]
    Base64Error(#[from] base64::DecodeError),
}
