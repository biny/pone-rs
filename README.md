# Pid One

Status: Alpha

## Features

### One

* Starts a command specified in the command line as child process.
* Forwards SIGTERM, SIGINT and SIGQUIT to child processes.
* Handles SIGCHLD.
* Exits when the started child process exits.

### Full

* Starts specified commands as child processes.
* Listens for HTTP and forwards requests via NNG to processes started with specified command.
