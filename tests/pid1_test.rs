const CHILD_PY: &'static str = "\
#!/usr/bin/python3
import os
import signal
import sys
import time

pid = os.fork()
if pid > 0:
    print(os.getpid())
    print(pid)
    sys.exit(0)
pid = os.fork()
if pid > 0:
    print(pid)
    sys.exit(0)
os.close(1)
signal.signal(signal.SIGINT, signal.SIG_DFL)
r = int(sys.stdin.read())
os.close(0)
os.close(2)
sys.exit(r)
";

mod pid1_test {

    use std::os::unix::prelude::*;

    use futures::prelude::*;
    // use futures::compat::*;

    use pone::for_main::*;
    use pone::{CoreSubsystem, CoreSubsystemRuntime};

    #[derive(thiserror::Error, Debug)]
    enum Error {
        #[error("{0}")]
        IoError(#[from] std::io::Error),
        #[error("{0}")]
        FromUtf8Error(#[from] std::string::FromUtf8Error),
        #[error("{0}")]
        ParseIntError(#[from] std::num::ParseIntError),
        #[error("{0}")]
        PoneError(#[from] pone::Error),
    }

    async fn write_child_py() -> Result<(), Error> {
        let py = "/tmp/child.py";
        tokio::fs::write(py, super::CHILD_PY).await?;
        tokio::fs::set_permissions(py, std::fs::Permissions::from_mode(0o755)).await?;
        Ok(())
    }

    async fn read_to_end<A: AsyncRead + Unpin>(
        mut a: A,
    ) -> Result<(A, Vec<u8>), futures::io::Error> {
        let mut buf = vec![];
        a.read_to_end(&mut buf).await?;
        Ok((a, buf))
    }

    async fn test_child<T>(terminate: T, reason: pone::PidExitReason) -> Result<(), Error>
    where
        T: FnOnce(
            u32,
            futures::io::AllowStdIo<std::process::ChildStdin>,
        ) -> std::pin::Pin<Box<dyn Future<Output = Result<(), Error>>>>,
        T: 'static,
    {
        if std::process::id() != 1 {
            return Ok(());
        }

        write_child_py().await?;

        let mut subsystems = SubsystemsBuild::new();
        subsystems.init_core();

        let runtime = subsystems.start().await?;

        let child = std::process::Command::new("/tmp/child.py")
            .stdin(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;

        let stdin = futures::io::AllowStdIo::new(child.stdin.unwrap());
        let (_, out) = read_to_end(futures::io::AllowStdIo::new(child.stdout.unwrap())).await?;
        let stderr = futures::io::AllowStdIo::new(child.stderr.unwrap());

        let out = String::from_utf8(out)?;
        dbg!(&out);
        let pids = out
            .lines()
            .map(|line| line.parse())
            .collect::<Result<Vec<u32>, _>>()?;
        assert_eq!(pids.len(), 3, "{:?}", pids);
        let pid2: u32 = pids[2];

        let waitpid2 = runtime.waitpid(pid2);

        dbg!(&pid2);
        terminate(pid2, stdin).await?;

        let (_, err) = read_to_end(stderr).await?;
        let err = String::from_utf8(err)?;
        dbg!(&err);

        let exit2 = waitpid2.await?;
        assert_eq!(exit2, Some(pone::PidExit { pid: pid2, reason }));

        Ok(())
    }

    #[tokio::test]
    async fn orphan_exit_0() -> Result<(), Error> {
        test_child(
            |_pid, mut stdin| {
                async move {
                    stdin.write_all("0".as_bytes()).await?;
                    Ok(())
                }
                .boxed_local()
            },
            pone::PidExitReason::Code(0),
        )
        .await
    }

    #[tokio::test]
    async fn orphan_exit_42() -> Result<(), Error> {
        test_child(
            |_pid, mut stdin| {
                async move {
                    stdin.write_all("42".as_bytes()).await?;
                    Ok(())
                }
                .boxed()
            },
            pone::PidExitReason::Code(42),
        )
        .await
    }

    #[tokio::test]
    async fn orphan_kill_sigterm() -> Result<(), Error> {
        test_child(
            |pid, stdin| {
                stdin.into_inner().into_raw_fd();
                unsafe {
                    libc::kill(pid as i32, libc::SIGTERM);
                }
                Box::pin(future::ok(()))
            },
            pone::PidExitReason::Signal(libc::SIGTERM),
        )
        .await
    }

    // #[tokio::test]
    // async fn pone_kill_sigterm() -> Result<(), Error> {
    //     test_child(
    //         |_pid, stdin| {
    //             stdin.into_inner().into_raw_fd();
    //             unsafe { libc::kill(std::process::id() as i32, libc::SIGTERM); }
    //             Box::pin(future::ok(()))
    //         },
    //         pone::PidExitReason::Signal(libc::SIGTERM)
    //     ).await
    // }

    // #[tokio::test]
    // async fn pone_kill_sigint() -> Result<(), Error> {
    //     test_child(
    //         |_pid, stdin| {
    //             stdin.into_inner().into_raw_fd();
    //             unsafe { libc::kill(std::process::id() as i32, libc::SIGINT); }
    //             Box::pin(future::ok(()))
    //         },
    //         pone::PidExitReason::Signal(libc::SIGINT)
    //     ).await
    // }

    // #[tokio::test]
    // async fn pone_kill_sigquit() -> Result<(), Error> {
    //     test_child(
    //         |_pid, stdin| {
    //             stdin.into_inner().into_raw_fd();
    //             unsafe { libc::kill(std::process::id() as i32, libc::SIGQUIT); }
    //             Box::pin(future::ok(()))
    //         },
    //         pone::PidExitReason::Signal(libc::SIGQUIT)
    //     ).await
    // }
}
